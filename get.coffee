get = (file, callback)->
  console.log "file: #{file}..."
  setTimeout ->
    console.log "file: #{file} complete"
    callback null, "(#{file})"
  , 200 + Math.random() * 100

module.exports = get
