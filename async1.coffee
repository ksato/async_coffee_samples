get = require './get'
async = require 'async'

async.map ['a.txt', 'b.txt', 'c.txt'], get, (err, result)->
  if err
    console.log err
    return -1
  console.log result.join ""
