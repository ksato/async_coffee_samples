get = require './get_promise'

get 'a.txt'
.then (result)->
  console.log result
.catch (error)->
  console.log "error", error
