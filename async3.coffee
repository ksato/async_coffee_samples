get = require './get'
async = require 'async'

async.series [
  (cb)->
    async.map ['a.txt', 'b.txt'], get, (err, result)->
      cb err, result.join ""
 ,(cb)->
    get 'c.txt', cb
], (err, results)->
  console.log results.join ""
