get = require './get_promise'

results = []
get 'a.txt'
.then (result)->
  results.push result
  get 'b.txt'
.then (result)->
  results.push result
  get 'c.txt'
.then (result)->
  results.push result
  console.log results.join ""


# recordValue = (results, value)->
#   results.push value
#   console.log results, value
#   return results
#
# pushValue = recordValue.bind null, []
#


# p = ['a.txt', 'b.txt', 'c.txt'].reduce (promise, txt)->
#   console.log promise, txt
#   promise.then get txt
#   .then (result)->
#     console.log 'result', result
#     results.push result
# , Promise.resolve()
#
# p.then (result)->
#   console.log results
#   console.log (results.join "") + result
#



# .then pushValue
# .then get 'b.txt'
# .then pushValue
# .then get 'c.txt'
# .then (results)->
#   console.log results

# a = (get(txt).then pushValue for txt in ['a.txt', 'b.txt', 'c.txt'])
# console.log a
# a = ['a.txt', 'b.txt', 'c.txt'].reduce (promise, txt)->
#   console.log promise, txt
#   promise.then get txt
#   .then pushValue
# , Promise.resolve()

# a.then (results)->
#   console.log results
