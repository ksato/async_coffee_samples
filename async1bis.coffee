get = require './get'
async = require 'async'

async.map ['a.txt', 'b.txt', 'c.txt']
, (item, callback)->
  get item, callback
, (err, result)->
  if err
    console.log err
    return -1
  console.log result.join ""
