get = (file)->
  console.log "file: #{file}..."
  new Promise (resolve, reject)->
    if file is 'd.txt'
      reject 'd is not acceptable'
      return
    setTimeout ->
      console.log "file: #{file} complete"
      resolve "(#{file})"
    , 200 + Math.random() * 100

module.exports = get
