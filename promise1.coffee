get = require './get_promise'

Promise.all ['a.txt', 'b.txt', 'c.txt'].map get
.then (results)->
  console.log results.join ""
.catch (error)->
  console.log "error", error
