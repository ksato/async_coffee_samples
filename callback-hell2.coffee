get = require './get'

n = 0
res = {}

++n
get 'a.txt', (err, a)->
  res.a = a
  if --n is 0
    callback null, res

++n
get 'b.txt', (err, b)->
  res.b = b
  if --n is 0
    callback null, res

++n
get 'c.txt', (err, c)->
  res.c = c
  if --n is 0
    callback null, res

callback = (err, res)->
  console.log res.a + res.b + res.c
